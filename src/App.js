import React from "react";
import { Route, Switch } from "react-router-dom";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/core";

import HomePage from "./screens/home/index.jsx";

import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";
import navReducer from "./redux/reducers/navReducer";
import userReducer from "./redux/reducers/userReducer";
import pageReducer from "./redux/reducers/pageReducer";

import "./App.scss";

const rootReducer = combineReducers({
  navReducer,
  userReducer,
  pageReducer,
});

const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Switch>
          <Route exact path="/" component={HomePage} />
        </Switch>
      </Provider>
    </ThemeProvider>
  );
}

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#A8A7FA",
    },
    secondary: {
      main: "#FF6565",
    },
  },
  typography: {
    fontFamily: "Roboto",
    h1: {
      fontFamily: "Roboto",
    },
    h2: {
      fontFamily: "circe",
      lineHeight: "3rem",
      fontWeight: "600",
      fontSize: "3rem",
    },
    h3: {
      fontFamily: "circe",
      //lineHeight: "3rem",
      fontWeight: "600",
      fontSize: "1.5rem",
    },
    h4: {
      fontFamily: "circe",
      fontSize: "2.5rem",
      fontWeight: "200",
    },
    h5: {
      fontFamily: "din-condensed",
      fontStyle: "normal",
      fontWeight: "700",
      lineHeight: "15px",
      size: "14px",
    },
    h6: {
      fontFamily: "din-condensed",
      fontWeight: "700",
      lineHeight: "17px",
      size: "17px",
    },
    subtitle1: {
      fontFamily: "circle",
      fontSize: "1.3rem",
      fontWeight: "400",
      lineHeight: "30px",
      color: "#6A6A6A",
    },
    subtitle2: {
      fontFamily: "Roboto",
      //color: "#555555",
      fontSize: "15px",
    },
    body1: {
      //fontFamily: "circle",
      //fontSize: "1.2rem",
      //fontWeight: "400",
      //color: "#6A6A6A",
    },
    body2: {
      fontFamily: "Roboto",
      //color: "#555555",
      fontSize: "15px",
    },
    button: {
      fontFamily: "din-condensed",
      fontSize: "1.1rem",
      fontWeight: "700",
      letterSpacing: "0.3px",
    },
  },
});

export default App;
