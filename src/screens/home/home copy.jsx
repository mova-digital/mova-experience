import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import svg from "../../assets/shapes/bg.svg";
import { Spring, config } from "react-spring/renderprops";
import { useSpring, animated } from "react-spring";
import { connect } from "react-redux";
import PageIndicator from "../../components/page-indicator/indicator";
import Box from "@material-ui/core/Box";

import Lottie from "react-lottie";
import animationData from "../../assets/lottie1.json";

import Nav from "../../components/nav/nav.component";

const SectionHome = (props) => {
  const classes = useStyles();

  const translate = useSpring({
    from: { transform: "translateY(0rem)" },
    to: async (next) => {
      while (2) {
        await next({ transform: "translateY(-100%)" });
      }
    },
    config: { tension: 200, friction: 40, mass: 2 },
  });

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div
      style={{ width: "100vw", overflow: "hidden", backgroundColor: "#A8A7FA" }}
    >
      <Nav />
      <PageIndicator />

      <div style={{ position: "relative" }}>
        <div style={{ position: "relative" }}>
          <svg
            //width="50rem"
            height="200%"
            viewBox="-450 0 1400 1400"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
          >
            <animated.g
              stroke="none"
              stroke-width="1"
              fill="none"
              fill-rule="evenodd"
              //style={translate}
            >
              <g
                transform="translate(-522.000000, 0.000000)"
                fill="#fff"
                fillOpacity="0.6"
              >
                <path
                  d="M580.744343,1536.5254 L769,1536.5254 C769,1185.5366 769,929.451376 769,768.269742 C769,611.999683 769,355.909769 769,-2.27373675e-13 L580.744343,-2.27373675e-13 C628.088434,97.815121 651.760479,222.682005 651.760479,374.600652 C651.760479,602.478621 522,905.457584 522,1160.33905 C522,1330.26002 541.581448,1455.65547 580.744343,1536.5254 Z"
                  id="Oval"
                  transform="translate(645.500000, 768.262699) scale(-1, -1) translate(-645.500000, -768.262699) "
                />
              </g>
            </animated.g>
          </svg>
        </div>
      </div>

      <div
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          height: "100%",
          width: "100%",
          //backgroundColor: "#f75052",
        }}
      >
        <Container container maxWidth="lg" className={classes.root}>
          <Grid container className={classes.wrapper}>
            <Box clone xs={12} md={6} order={{ xs: 2, md: 1 }}>
              <Grid item xs={12} className={classes.gridItem}>
                <div className={classes.textWrapper}>
                  <Typography color="primary" variant="h2" align="inherit">
                    We build the best
                  </Typography>
                  <Typography color="primary" variant="h2">
                    user expereriences.
                  </Typography>
                  <div className={classes.heroText}>
                    <Typography variant="subtitle1" align="inherit">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua.
                    </Typography>
                  </div>
                </div>
              </Grid>
            </Box>

            <Box clone xs={12} md={6} order={{ xs: 1, md: 2 }}>
              <Grid item className={classes.gridItem}>
                <div className={classes.lottieWrapper}>
                  <Lottie
                    options={defaultOptions}
                    height={"100%"}
                    width={"100%"}
                  />
                </div>
              </Grid>
            </Box>
          </Grid>
        </Container>
      </div>
    </div>
  );
};

// Map the state of the store to the props to be passed
// to the container component.
const mapStateToProps = (state) => ({});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionHome);

const useStyles = makeStyles((theme) => ({
  circle: {
    transformOrigin: "center",
    zIndex: 200,
  },

  root: {
    height: "100%",
    width: "100%",
  },
  wrapper: {
    height: "100%",
    width: "100%",

    [theme.breakpoints.down("md")]: {
      padding: "30px 0",
    },
  },
  gridItem: {
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    //backgroundColor: "#9e42b0",
    padding: "0 2rem",

    [theme.breakpoints.down("sm")]: {
      height: "40%",
    },
  },
  textWrapper: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    textAlign: "left",

    [theme.breakpoints.down("md")]: {
      textAlign: "center",
    },
  },
  heroText: {
    maxWidth: "30rem",
    marginTop: "10px",
    //textAlign: "left",
  },
}));

/*
      <svg
        width="50vw"
        height="50vh"
        viewBox="150 100 200 200"
        style={{ border: "3px solid #0075A2" }}
      >
        <animated.circle r="100" cx="100" cy="100" style={spring} />
      </svg> */

/* <Container container maxWidth="lg" className={classes.root}>
          <Grid container className={classes.wrapper}>
            <Box clone xs={12} md={6} order={{ xs: 2, md: 1 }}>
              <Grid item xs={12} className={classes.gridItem}>
                <div className={classes.textWrapper}>
                  <Typography color="primary" variant="h2" align="inherit">
                    We build the best
                  </Typography>
                  <Typography color="primary" variant="h2">
                    user expereriences.
                  </Typography>
                  <div className={classes.heroText}>
                    <Typography variant="subtitle1" align="inherit">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua.
                    </Typography>
                  </div>
                </div>
              </Grid>
            </Box>

            <Box clone xs={12} md={6} order={{ xs: 1, md: 2 }}>
              <Grid item className={classes.gridItem}>
                <div className={classes.lottieWrapper}>
                  <Lottie
                    options={defaultOptions}
                    height={"100%"}
                    width={"100%"}
                  />
                </div>
              </Grid>
            </Box>
          </Grid>
        </Container>

        */
