import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Rotator from "../../components/svg/rotateSVG";
import Nav from "../../components/nav/nav.component.jsx";
import PageIndicator from "../../components/page-indicator/indicator";

import { useSpring, animated } from "react-spring";

const PageTwo = () => {
  const classes = useStyles();

  const spring = useSpring({
    from: { opacity: 0 },
    to: { opacity: 1 },
  });

  return (
    <div>
      <div style={{ height: "100vh", width: "100vw", overflow: "hidden" }}>
        <Nav />
        <PageIndicator />

        <div className={classes.backgroundSvg}>
          <Rotator />
        </div>

        <Container container maxWidth="lg" className={classes.heroWrapper}>
          <Grid container spacing={3} className={classes.heroGrid}>
            <Grid item xs={6} className={classes.gridItem}>
              <div>
                <div className={classes.colTwo}>
                  <Typography align="left" color="primary" variant="h2">
                    Lorem ipsu dolor site amet
                  </Typography>
                  <div className={classes.heroText}>
                    <Typography align="left" variant="subtitle1">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing
                      elit, sed do eiusmod tempor incididunt ut labore et dolore
                      magna aliqua.
                    </Typography>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  );
};

export default PageTwo;

const useStyles = makeStyles((theme) => ({
  backgroundSvg: {
    height: "100%",
    //width: "100%",
    position: "fixed",
    right: "0",
    top: "0",
    //padding: "10rem ",
  },
  root: {
    height: "100vh",
  },
  heroWrapper: {
    height: "100%",
  },
  heroGrid: {
    height: "100%",
  },
  gridItem: {
    height: "100%",
    display: "flex",
    alignItems: "center",
  },
}));
