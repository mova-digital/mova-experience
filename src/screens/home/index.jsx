import React, { useState, useEffect, Component } from "react";
import { makeStyles } from "@material-ui/core/styles";

import SectionOne from "./section1";
import SectionTwo from "./section2";
import SectionThree from "./section3";
import SectionFooter from "./sectionFooter";
import SectionHome from "./home.jsx";

import { debounce } from "lodash";
import { connect } from "react-redux";
import { render } from "@testing-library/react";

class HomePage extends Component {
  componentDidMount() {
    this.debouncedScroll = debounce((e) => {
      this.handleScroll(e);
    }, 300);

    window.addEventListener("wheel", (event) => this.debouncedScroll(event));
    window.addEventListener("keyup", (event) => this.handleKeyUp(event));
  }

  componentDidUnmount() {
    window.removeEventListener("wheel", (event) => this.debouncedScroll(event));
    window.removeEventListener("keyup", (event) => this.handleKeyUp(event));
  }

  handleDecrementPage() {
    const newNavState = this.props.page - 1;
    if (newNavState < 0) {
      this.props.setPageState(0);
    } else {
      this.props.setPageState(newNavState);
    }
  }

  handleIncremenetPage() {
    const newNavState = this.props.page + 1;
    console.log(newNavState);
    if (newNavState >= 3) {
      this.props.setPageState(3);
    } else {
      this.props.setPageState(newNavState);
    }
  }

  handleKeyUp(e) {
    let keyPress = e.key;

    console.log(keyPress);

    if (keyPress == "ArrowDown" || keyPress == "ArrowRight") {
      this.handleIncremenetPage();
    } else if (keyPress == "ArrowUp" || keyPress == "ArrowLeft") {
      this.handleDecrementPage();
    }
  }

  handleScroll(e) {
    let deltaY = e.deltaY;

    if (deltaY > 0) {
      this.handleIncremenetPage();
    } else if (deltaY < 0) {
      this.handleDecrementPage();
    }
  }

  render() {
    let dynamicSection = <SectionHome />;

    if (this.props.page === 1) {
      dynamicSection = <SectionTwo />;
    } else if (this.props.page === 2) {
      dynamicSection = <SectionThree />;
    } else if (this.props.page === 3) {
      dynamicSection = <SectionFooter />;
    }

    return <div>{dynamicSection}</div>;
  }
}

// Map the state of the store to the props
const mapStateToProps = (state) => ({
  page: state.pageReducer.page,
});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({
  setPageState: (page) =>
    dispatch({
      type: "UPDATE_PAGE",
      payload: page,
    }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);

// const useStyles = makeStyles((theme) => ({
//   root: {},
// }));

/*
const handlePageNav = debounce((e) => {
      let deltaY = e.deltaY;

      if (deltaY > 0) {
        const newNavState = props.page + 1;
        console.log(newNavState);

        if (newNavState >= 3) {
          props.setPageState(3);
        } else {
          props.setPageState(newNavState);
        }
      } else {
        const newNavState = props.page - 1;
        console.log(newNavState);

        if (newNavState <= 0) {
          props.setPageState(0);
        } else {
          props.setPageState(newNavState);
        }
      }
    }, 400);
 if (props.page + 1 <= 0) {
          props.setPageState(0);
        } else {
          props.setPageState(props.page + 1);
        }
if (props.page >= 3) {
          props.setPageState(0);
          //console.log(keyState);
        } else {
          props.setPageState(props.page + 1);
        }
else if (keyPress == "ArrowUp") {
        const keyState = props.page - 1;

        if (keyState < 0) {
          props.setPageState(0);
        } else {
          props.setPageState(keyState);
        }
      }

const onMouseMove = debounce((e) => {
      //console.log(e);
      if (e.deltaY > 0) {
        const newScrollState = props.page + 1;
        if (newScrollState > 3) {
          props.setPageState(3);
        } else {
          props.setPageState(newScrollState);
        }
      } else {
        const newScrollState = props.page - 1;
        if (newScrollState < 0) {
          props.setPageState(0);
        } else {
          props.setPageState(newScrollState);
        }
      }
    }, 200);
    
useEffect(() => {
    const onMouseMove = debounce((e) => {
      //console.log(e);
      if (e.deltaY > 0) {
        const newScrollState = prevScrollState + 1;
        if (newScrollState > 3) {
          props.setPageState(3);
          setPrevScrollState(3);
        } else {
          setPrevScrollState(newScrollState);
        }
        //console.log(prevScrollState);
      } else {
        const newScrollState = prevScrollState - 1;
        if (newScrollState < 0) {
          props.setPageState(0);

          setPrevScrollState(0);
        } else {
          setPrevScrollState(newScrollState);
          props.setPageState(newScrollState);
        }
        //console.log(scrollState);
      }
    }, 200);
    window.addEventListener("wheel", onMouseMove);
    //console.log(prevScrollState);

    return () => {
      window.removeEventListener("wheel", onMouseMove);
    };
  });
  */
