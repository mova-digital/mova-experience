import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import svg from "../../assets/shapes/bg.svg";
import { Spring, config } from "react-spring/renderprops";
import { useSpring, animated } from "react-spring";
import { connect } from "react-redux";
import PageIndicator from "../../components/page-indicator/indicator";
import Box from "@material-ui/core/Box";

import Lottie from "react-lottie";
import animationData from "../../assets/lottie1.json";

import Nav from "../../components/nav/nav.component";

const SectionHome = (props) => {
  const classes = useStyles();

  const animationOne = useSpring({
    from: { transform: "translateY(5rem)" },
    to: async (next) => {
      while (2) {
        await next({ transform: "translateY(-20rem)" });
        await next({ transform: "translateY(10rem)" });
        await next({ transform: "translateY(-20rem)" });
      }
    },
    config: { tension: 500, friction: 100, mass: 100, delay: 5 },
  });

  const animationTwo = useSpring({
    from: { transform: "translateY(-5rem)" },
    to: async (next) => {
      while (2) {
        await next({ transform: "translateY(20rem)" });
        await next({ transform: "translateY(-10rem)" });
        await next({ transform: "translateY(20rem)" });
      }
    },
    config: { tension: 500, friction: 100, mass: 100, delay: 5 },
  });

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div
      style={{
        height: "100vh",
        width: "100vw",
        overflow: "hidden",
        backgroundColor: "#A8A7FA",
      }}
    >
      <Nav />
      <PageIndicator />

      <div className={classes.background}>
        <div className={classes.colOne} />
        <div className={classes.colTwo} />
      </div>
      <div className={classes.waveWrapper}>
        <svg
          width="200px"
          height="100vh"
          viewBox="0 1000 446 2000"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
        >
          <animated.g
            id="Page-1"
            stroke="none"
            stroke-width="1"
            fill="none"
            fill-rule="evenodd"
            fill-opacity=".4"
            transform="translate(80.000000, 0.000000)"
            style={animationOne}
          >
            <g id="Artboard" fill="#fff">
              <path
                d="M0,0 L446,0 C230.034262,707.083333 122.051393,1422.91667 122.051393,2147.5 C122.051393,2872.08333 230.034262,3604.58333 446,4345 L0,4345 L0,0 Z"
                id="Rectangle"
              />
            </g>
          </animated.g>
          <animated.g
            id="Page-1"
            stroke="none"
            stroke-width="1"
            fill="none"
            fill-rule="evenodd"
            fill-opacity=".4"
            transform="translate(40.000000, 0.000000)"
            style={animationTwo}
          >
            <g id="Artboard" fill="#fff">
              <path
                d="M0,0 L446,0 C230.034262,707.083333 122.051393,1422.91667 122.051393,2147.5 C122.051393,2872.08333 230.034262,3604.58333 446,4345 L0,4345 L0,0 Z"
                id="Rectangle"
              />
            </g>
          </animated.g>
          <g
            id="Page-1"
            stroke="none"
            stroke-width="1"
            fill="none"
            fill-rule="evenodd"
            fill-opacity="1"
            transform="translate(00.000000, 0.000000)"
          >
            <g id="Artboard" fill="#fff">
              <path
                d="M0,0 L446,0 C230.034262,707.083333 122.051393,1422.91667 122.051393,2147.5 C122.051393,2872.08333 230.034262,3604.58333 446,4345 L0,4345 L0,0 Z"
                id="Rectangle"
              />
            </g>
          </g>
        </svg>
      </div>

      <div
        style={{
          posotion: "absolute",
          top: 0,
          left: 0,
          width: "100vw",
          height: "100vh",
          backgroundColor: "#4fe4b1",
          display: "flex",
          justifyContent: "space-around",
        }}
      >
        <div className={classes.root}>
          <Container container maxWidth="lg" style={{ height: "100%" }}>
            <Grid container className={classes.wrapper}>
              <Box clone xs={12} md={6} order={{ xs: 2, md: 1 }}>
                <Grid item xs={12} className={classes.gridItem}>
                  <div className={classes.textWrapper}>
                    <Typography color="primary" variant="h2" align="inherit">
                      We build the best
                    </Typography>
                    <Typography color="primary" variant="h2">
                      user expereriences.
                    </Typography>
                    <div className={classes.heroText}>
                      <Typography variant="subtitle1" align="inherit">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua.
                      </Typography>
                    </div>
                  </div>
                </Grid>
              </Box>

              <Box clone xs={12} md={6} order={{ xs: 1, md: 2 }}>
                <Grid item className={classes.gridItem}>
                  <div className={classes.lottieWrapper}>
                    <Lottie
                      options={defaultOptions}
                      height={"100%"}
                      width={"100%"}
                    />
                  </div>
                </Grid>
              </Box>
            </Grid>
          </Container>
        </div>
      </div>
    </div>
  );
};

// Map the state of the store to the props to be passed
// to the container component.
const mapStateToProps = (state) => ({});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionHome);

const useStyles = makeStyles((theme) => ({
  background: {
    display: "flex",
    height: "100%",
    position: "relative",
  },
  waveWrapper: {
    position: "absolute",
    top: 0,
    left: "45%",
    //backgroundColor: "#65e090",
    height: "100%",
    width: "8rem",
  },
  colOne: {
    backgroundColor: "#fff",
    flex: "1 45%",
    height: "100%",
  },
  colTwo: {
    backgroundColor: "#A8A7FA",
    flex: "1 55%",
    height: "100%",
  },
  root: {
    height: "100vh",
    width: "100vw",
    position: "absolute",
    zIndex: 3000,
    top: 0,
    left: 0,
    //backgroundColor: "#4fe4b1",
  },
  wrapper: {
    height: "100%",
    width: "100%",

    [theme.breakpoints.down("md")]: {
      padding: "30px 0",
    },
  },
  gridItem: {
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    //backgroundColor: "#9e42b0",
    padding: "0 2rem",

    [theme.breakpoints.down("sm")]: {
      height: "40%",
    },
  },
  textWrapper: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    textAlign: "left",

    [theme.breakpoints.down("md")]: {
      textAlign: "center",
    },
  },
  heroText: {
    maxWidth: "30rem",
    marginTop: "10px",
    //textAlign: "left",
  },
}));

/*
      <svg
        width="50vw"
        height="50vh"
        viewBox="150 100 200 200"
        style={{ border: "3px solid #0075A2" }}
      >
        <animated.circle r="100" cx="100" cy="100" style={spring} />
      </svg> */

/* <Container container maxWidth="lg" className={classes.root}>
          <Grid container className={classes.wrapper}>
            <Box clone xs={12} md={6} order={{ xs: 2, md: 1 }}>
              <Grid item xs={12} className={classes.gridItem}>
                <div className={classes.textWrapper}>
                  <Typography color="primary" variant="h2" align="inherit">
                    We build the best
                  </Typography>
                  <Typography color="primary" variant="h2">
                    user expereriences.
                  </Typography>
                  <div className={classes.heroText}>
                    <Typography variant="subtitle1" align="inherit">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua.
                    </Typography>
                  </div>
                </div>
              </Grid>
            </Box>

            <Box clone xs={12} md={6} order={{ xs: 1, md: 2 }}>
              <Grid item className={classes.gridItem}>
                <div className={classes.lottieWrapper}>
                  <Lottie
                    options={defaultOptions}
                    height={"100%"}
                    width={"100%"}
                  />
                </div>
              </Grid>
            </Box>
          </Grid>
        </Container>

        */
