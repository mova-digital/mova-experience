import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CTAInput from "../../components/input/input.component";
import FooterBackground from "../../assets/shapes/footer";

import Logo1 from "../../assets/logos/IrishExaminer.png";
import Logo2 from "../../assets/logos/EveningEcho.png";
import Logo3 from "../../assets/logos/SouthernStar.png";
import Logo4 from "../../assets/logos/UlsterBank.png";

import Nav from "../../components/nav/nav.component";

import { useSpring, animated } from "react-spring";
import PageIndicator from "../../components/page-indicator/indicator";

import Facebook from "../../assets/icons/facebook.svg";
import Twitter from "../../assets/icons/twitter.svg";
import Instagram from "../../assets/icons/instagram.svg";
import LinkedIn from "../../assets/icons/linkedin.svg";

const SectionFooter = () => {
  const classes = useStyles();
  const spring = useSpring({
    from: { opacity: 0 },
    to: { opacity: 1 },
  });

  return (
    <div>
      <div style={{ height: "100vh", width: "100vw", overflow: "hidden" }}>
        <Nav />
        <PageIndicator />

        <div className={classes.test}>
          <Container style={{ height: "100%" }}>
            <Typography align="center" variant="h3">
              Featured by:
            </Typography>
            <Grid
              container
              spacing={3}
              style={{ height: "100%" }}
              justify="center"
              alignItems="center"
            >
              <Grid item xs={3} style={{ textAlign: "center" }}>
                <img src={Logo1} className={classes.logo} />
              </Grid>
              <Grid item xs={3}>
                <img src={Logo2} className={classes.logo} />
              </Grid>
              <Grid item xs={3}>
                <img src={Logo3} className={classes.logo} />
              </Grid>
              <Grid item xs={3}>
                <img src={Logo4} className={classes.logo} />
              </Grid>
            </Grid>
          </Container>
        </div>

        <div className={classes.wrapper}>
          <div style={{ position: "relative" }}>
            <FooterBackground />
          </div>

          <div className={classes.footerText}>
            <Typography align="center" style={{ color: "#fff" }} variant="h2">
              Let's schedule a free call
            </Typography>
            <Typography
              align="center"
              variant="subtitle1"
              style={{ paddingTop: "1rem" }}
              style={{ color: "#fff" }}
            >
              Scheulde a quick call with one of our team members to bring your
              product to life.
            </Typography>
            <div style={{ paddingTop: "1rem" }}>
              <CTAInput />
            </div>
          </div>

          <div className={classes.footerWrapper}>
            <Container>
              <div className={classes.iconWrapper}>
                <div>
                  <img src={Facebook} className={classes.icon} />
                  <img src={LinkedIn} className={classes.icon} />
                  <img src={Twitter} className={classes.icon} />
                </div>
              </div>

              <div
                style={{
                  borderTop: "1px solid #fff",
                  padding: 15,
                }}
              >
                <Grid
                  container
                  spacing={2}
                  justify="center"
                  alignItems="center"
                >
                  <Grid item xs={3}>
                    <a href="#">
                      <Typography
                        align="center"
                        style={{ color: "#fff" }}
                        className={classes.FooterLink}
                        variant="h6"
                      >
                        Products
                      </Typography>
                    </a>
                  </Grid>
                  <Grid item xs={3}>
                    <a href="#">
                      <Typography
                        align="center"
                        style={{ color: "#fff" }}
                        className={classes.FooterLink}
                        variant="h6"
                      >
                        Customers
                      </Typography>
                    </a>
                  </Grid>
                  <Grid item xs={3}>
                    <a href="#">
                      <Typography
                        align="center"
                        style={{ color: "#fff" }}
                        className={classes.FooterLink}
                        variant="h6"
                      >
                        Contact us
                      </Typography>
                    </a>
                  </Grid>
                  <Grid item xs={3}>
                    <a href="#">
                      <Typography
                        align="center"
                        style={{ color: "#fff" }}
                        className={classes.FooterLink}
                        variant="h6"
                      >
                        Privacy Policy
                      </Typography>
                    </a>
                  </Grid>
                </Grid>
              </div>
            </Container>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionFooter;

const useStyles = makeStyles((theme) => ({
  logo: {
    width: "10rem",
    filter: "grayscale(100%)",
    cursor: "pointer",
    transition: "all .2s",
    zIndex: 3,
    position: "relative",

    "&:hover": {
      transform: "scale(1.2)",
    },
  },
  iconWrapper: {
    width: "100%",
    display: "flex",
    justifyContent: "space-around",
    padding: "15px 0",
  },
  icon: {
    height: "2rem",
    padding: "0 10px",
    transition: "all .2s",
    cursor: "pointer",
    //backgroundColor: "#85eb52",

    "&:hover": {
      transform: "scale(1.1)",
    },
  },
  backgroundSvg: {
    width: "100%",
    position: "fixed",
    left: "0",
    bottom: "-20%",
  },
  footerText: {
    //backgroundColor: "#6AE6EC",
    position: "absolute",
    top: "50%",
    left: "50%",
    zIndex: 5,
    transform: "translate(-50%, 50%)",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  FooterLink: {
    textDecoration: "none",
    transition: "all .2s",

    "&:hover": {
      transform: "scale(1.1)",
    },
  },
  footerWrapper: {
    zIndex: 2,
    position: "fixed",
    bottom: 0,
    left: 0,
    //backgroundColor: "#0E86B7",
    width: "100%",
    paddingBottom: "2rem",
  },
  footerLinks: {
    display: "flex",
  },
  test: {
    marginTop: "5rem",
    height: "20%",
  },
  wrapper: {
    height: "70%",
  },
  heroWrapper: {
    zIndex: 2,
  },
  heroGrid: {
    height: "100%",
  },
  gridItem: {
    height: "100%",
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "space-around",
    zIndex: 2,
  },
}));
