import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import svg from "../../assets/shapes/bg.svg";
import SectionOne from "../../components/svg/rotateSVG";
import { useSpring, animated, useTransition } from "react-spring";
import PageIndicator from "../../components/page-indicator/indicator";
import Lottie from "react-lottie";
import animationData from "../../assets/bg.json";

import icon1 from "../../assets/app-icons/Icon-1.svg";
import icon2 from "../../assets/app-icons/Icon-2.svg";
import icon3 from "../../assets/app-icons/Icon-3.svg";
import icon4 from "../../assets/app-icons/Icon-4.svg";
import icon5 from "../../assets/app-icons/Icon-5.svg";
import icon6 from "../../assets/app-icons/Icon-6.svg";
import icon7 from "../../assets/app-icons/Icon-7.svg";
import icon8 from "../../assets/app-icons/Icon-8.svg";
import icon9 from "../../assets/app-icons/Icon-9.svg";

import Nav from "../../components/nav/nav.component";

const PageThree = () => {
  const classes = useStyles();

  const [offSet, setOffSet] = useState(0);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  // Array of images
  const images = [
    {
      id: 0,
      url: "Icon-1.svg",
    },
    {
      id: 1,
      url: "Icon-2.svg",
    },
    {
      id: 2,
      url: "Icon-3.svg",
    },
    {
      id: 3,
      url: "Icon-4.svg",
    },
    {
      id: 4,
      url: "Icon-5.svg",
    },
    {
      id: 5,
      url: "Icon-6.svg",
    },
  ];

  for (var i = 1; i <= 10; i++) {
    (function(index) {
      setTimeout(function() {
        setOffSet(offSet + 200);
      }, 1000);
    })(i);
  }

  const { y } = useSpring({
    y: offSet,
  });

  /*
  const test = (el) => {
    // console.log(el.getBoundingClientRect());

    if (el.getBoundingClientRect().x > 100) {
      console.log("hello");
      images.unshift(...images, {
        id: 10,
        url: "Icon-4.svg",
      });
    }
  }; */

  useEffect(() => {});

  return (
    <div
      style={{
        //height: "100vh",
        width: "100vw",
        overflow: "hidden",
        display: "flex",
      }}
    >
      <Nav />
      <PageIndicator />

      <div className={classes.root}>
        <Lottie options={defaultOptions} height={"30%"} width={"100%"} />
        <div />

        <div style={{ width: "100vw" }}>
          <Container container maxWidth="lg" className={classes.textWrapper}>
            <Typography align="center" color="primary" variant="h2">
              What makes our products so special?
            </Typography>

            <div className={classes.heroText}>
              <Typography align="center" variant="subtitle1">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </Typography>
            </div>
          </Container>
        </div>
        <Lottie options={defaultOptions} height={200} width={"100%"} />
      </div>
    </div>
  );
};

export default PageThree;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    //backgroundColor: "#FF0B0B",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    padding: "8rem 0",
  },
  heroGrid: {
    //height: "100%",
    //paddingTop: "5rem",
    backgroundColor: "#862B2B",
    width: "200%",
  },
  icon: {
    width: "12rem",
    height: "12rem",
    padding: "0 3rem",
    zIndex: 3,
  },
  textWrapper: {
    padding: "3rem 0",
  },
}));

//<Nav />
//<Header />

/*
<div className={classes.heroGrid}>
            {images.map((item) => (
              <animated.img
                style={{
                  transform: y.interpolate((y) => `translateX(${y}px)`),
                }}
                key={item.id}
                className={classes.icon}
                ref={(el) => {
                  if (!el) return;
                  test(el);
                }}
                src={`https://jamesgale.s3-eu-west-1.amazonaws.com/icons/${
                  item.url
                }`}
              />
            ))}
          </div>
<div className={classes.heroGrid}>
          <img src={icon1} className={classes.icon} />
          <img src={icon2} className={classes.icon} />
          <img src={icon9} className={classes.icon} />
          <img src={icon3} className={classes.icon} />
          <img src={icon8} className={classes.icon} />
          <img src={icon4} className={classes.icon} />
          <img src={icon7} className={classes.icon} />
        </div> */
