import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import svg from "../../assets/shapes/bg.svg";
import { useSpring, animated } from "react-spring";
import { connect } from "react-redux";
import PageIndicator from "../../components/page-indicator/indicator";

import Box from "@material-ui/core/Box";

import Lottie from "react-lottie";
import animationData from "../../assets/data.json";

import Nav from "../../components/nav/nav.component";

const SectionOne = (props) => {
  const classes = useStyles();

  const spring = useSpring({
    from: { transform: "rotate(0deg)" },
    to: async (next) => {
      while (1) {
        await next({ transform: "rotate(360deg)" });
        await next({ transform: "rotate(360deg)" });
      }
    },
    config: { duration: "100000" },
  });

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div style={{ height: "100vh", overflow: "hidden" }}>
      <Grid container spacing={0} style={{ height: "100%" }} item="true">
        <Box clone xs={12} md={6} order={{ xs: 2, md: 1 }}>
          <Grid itemScope>Col 1 </Grid>
        </Box>
        <Box clone xs={12} md={6} order={{ xs: 1, md: 2 }}>
          <Grid item className={classes.colRightWrapper}>
            Col 2
            <div className={classes.colRight}>
              <div>
                <Lottie options={defaultOptions} height={400} width={400} />

                <animated.img
                  src={svg}
                  className={classes.backgroundSvg}
                  style={spring}
                />
              </div>
            </div>
          </Grid>
        </Box>
      </Grid>
    </div>
  );
};

// Map the state of the store to the props to be passed
// to the container component.
const mapStateToProps = (state) => ({});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionOne);

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "row",
    backgroundColor: "#2647FE",
  },
  colLeft: {
    backgroundColor: "#2647FE",
    [theme.breakpoints.down("sm")]: {
      height: "50%",
    },
  },
  colRightWrapper: {
    height: "100%",
    [theme.breakpoints.down("sm")]: {
      height: "50%",
    },
  },
  colRight: {
    position: "static",
    backgroundColor: "#F5E5FC",
    height: "100%",
  },
  backgroundSvg: {
    height: "200%",

    position: "relative",
    top: "-50%",
    left: 0,
    transform: "translate(0, 0)",

    [theme.breakpoints.down("sm")]: {
      height: "110%",
      top: 0,
      left: "50%",
      transform: "translate(-50%, -50%)",
    },
  },
}));

/*
<animated.img
          src={svg}
          className={classes.backgroundSvg}
          //style={spring}
        />

        <Container container maxWidth="lg" className={classes.root}>
          <Grid container className={classes.wrapper}>
            <Grid item xs={12} lg={6} className={classes.gridItem}>
              <div className={classes.textWrapper}>
                <Typography color="primary" variant="h2" align="inherit">
                  We build the best
                </Typography>
                <Typography color="primary" variant="h2">
                  user expereriences.
                </Typography>
                <div className={classes.heroText}>
                  <Typography variant="subtitle1" align="inherit">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua.
                  </Typography>
                </div>
              </div>
            </Grid>
          </Grid>
        </Container>

        */
