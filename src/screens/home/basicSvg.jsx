import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import svg from "../../assets/shapes/bg.svg";
import { useSpring, animated } from "react-spring";
import { connect } from "react-redux";
import PageIndicator from "../../components/page-indicator/indicator";
import Box from "@material-ui/core/Box";

import Lottie from "react-lottie";
import animationData from "../../assets/lottie1.json";

import Nav from "../../components/nav/nav.component";

const SectionHome = (props) => {
  const classes = useStyles();

  const spring = useSpring({
    from: { opacity: 0 },
    to: { opacity: 1 },
  });

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div>
      <svg
        width="50vw"
        height="50vh"
        viewBox="150 100 200 200"
        style={{ border: "3px solid #0075A2" }}
      >
        <animated.circle r="100" cx="100" cy="100" style={spring} />
      </svg>
    </div>
  );
};

// Map the state of the store to the props to be passed
// to the container component.
const mapStateToProps = (state) => ({});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionHome);

const useStyles = makeStyles((theme) => ({
  textWrapper: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    textAlign: "left",

    [theme.breakpoints.down("md")]: {
      textAlign: "center",
    },
  },
  heroText: {
    maxWidth: "30rem",
    marginTop: "10px",
    //textAlign: "left",
  },
}));
