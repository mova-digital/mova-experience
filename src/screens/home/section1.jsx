import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import svg from "../../assets/shapes/bg.svg";
import { useSpring, animated } from "react-spring";
import { connect } from "react-redux";
import PageIndicator from "../../components/page-indicator/indicator";
import Box from "@material-ui/core/Box";

import Lottie from "react-lottie";
import animationData from "../../assets/lottie1.json";

import Nav from "../../components/nav/nav.component";

const SectionOne = (props) => {
  const classes = useStyles();

  const spring = useSpring({
    from: { transform: "rotate(0deg)" },
    to: async (next) => {
      while (1) {
        await next({ transform: "rotate(360deg)" });
        await next({ transform: "rotate(360deg)" });
      }
    },
    config: { duration: "100000" },
  });

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div>
      <div
        style={{
          height: "100vh",
          width: "100vw",
          overflow: "hidden",
          backgroundColor: "#63A46C",
        }}
      >
        <animated.img
          src={svg}
          className={classes.backgroundSvg}
          style={spring}
        />
        <Nav />
        <PageIndicator />

        <Container container maxWidth="lg" className={classes.root}>
          <Grid container className={classes.wrapper}>
            <Box clone xs={12} md={6} order={{ xs: 2, md: 1 }}>
              <Grid item xs={12} className={classes.gridItem}>
                <div className={classes.textWrapper}>
                  <Typography color="primary" variant="h2" align="inherit">
                    We build the best
                  </Typography>
                  <Typography color="primary" variant="h2">
                    user expereriences.
                  </Typography>
                  <div className={classes.heroText}>
                    <Typography variant="subtitle1" align="inherit">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua.
                    </Typography>
                  </div>
                </div>
              </Grid>
            </Box>

            <Box clone xs={12} md={6} order={{ xs: 1, md: 2 }}>
              <Grid item className={classes.gridItem}>
                <div className={classes.lottieWrapper}>
                  <Lottie
                    options={defaultOptions}
                    height={"100%"}
                    width={"100%"}
                  />
                </div>
              </Grid>
            </Box>
          </Grid>
        </Container>
      </div>
    </div>
  );
};

// Map the state of the store to the props to be passed
// to the container component.
const mapStateToProps = (state) => ({});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SectionOne);

const useStyles = makeStyles((theme) => ({
  backgroundSvg: {
    width: "100rem",
    position: "absolute",
    left: "50%",
    top: "50%",
    //top: "-60rem",
    transform: "translate(0, -50%)",
    overflow: "hidden",

    [theme.breakpoints.down("md")]: {
      left: "50%",
      top: "-75rem",
      transform: "translate(-50%, 0)",
    },
  },
  lottieWrapper: {
    zIndex: 200,
    padding: 0,
  },
  root: {
    height: "100%",
    width: "100%",
  },
  wrapper: {
    height: "100%",
    width: "100%",

    [theme.breakpoints.down("md")]: {
      padding: "30px 0",
    },
  },
  gridItem: {
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    //backgroundColor: "#9e42b0",
    padding: "0 2rem",

    [theme.breakpoints.down("sm")]: {
      height: "40%",
    },
  },
  textWrapper: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    textAlign: "left",

    [theme.breakpoints.down("md")]: {
      textAlign: "center",
    },
  },
  heroText: {
    maxWidth: "30rem",
    marginTop: "10px",
    //textAlign: "left",
  },
}));

/*
        <animated.img
          src={svg}
          className={classes.backgroundSvg}
          //style={spring}
        />
        <animated.img
          src={svg}
          className={classes.backgroundSvg}
          style={spring}
        />
        */
