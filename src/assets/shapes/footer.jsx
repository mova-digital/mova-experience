import React, { useState } from "react";
import { useSpring, animated } from "react-spring";
import CTAInput from "../../components/input/input.component";

const FooterBackground = () => {
  const spring = useSpring({
    from: { transform: "rotate(0deg)" },
    to: async (next) => {
      while (1) {
        await next({ transform: "rotate(360deg)" });
        await next({ transform: "rotate(360deg)" });
      }
    },
    config: { duration: "100000" },
  });

  return (
    <animated.div style={spring}>
      <svg
        width="200%"
        height="100%"
        style={{ transform: "translate(-25%, 0)" }}
        viewBox="0 0 1721 1721"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-492.000000, -183.000000)">
            <g transform="translate(497.000000, 188.000000)">
              <circle fill="#A8A7FA" cx="856" cy="856" r="808" />
              <path
                d="M855.5,1711 C1327.9796,1711 1711,1327.9796 1711,855.5 C1711,383.020397 1327.9796,0 855.5,0 C383.020397,0 0,383.020397 0,855.5 C0,1327.9796 383.020397,1711 855.5,1711 Z"
                id="Oval"
                stroke="#E0E0E0"
                stroke-width="10"
                stroke-linecap="round"
                stroke-dasharray="14,40"
              />
            </g>
          </g>
        </g>
      </svg>
    </animated.div>
  );
};

export default FooterBackground;
