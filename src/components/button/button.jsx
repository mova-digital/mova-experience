import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { THEME_PRIMARY, COL_WHITE } from "../constants";
import { connect } from "react-redux";

const CustomButton = (props, { children, ...otherProps }) => {
  const classes = useStyles();

  let bgCol = COL_WHITE;
  let textCol = THEME_PRIMARY;

  if (props.navDark !== true) {
    bgCol = THEME_PRIMARY;
    textCol = COL_WHITE;
  }

  console.log(props);

  return (
    <Button
      {...otherProps}
      className={classes.wrapper}
      style={{ color: textCol, backgroundColor: bgCol }}
    >
      <span>
        {props.children}
        <svg
          width="30px"
          height="15px"
          viewBox="0 0 15 100"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
        >
          <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g fill={textCol}>
              <path
                d="M31.0349049,0.888769357 L31.4906912,1.28713012 L31.4906912,1.28713012 L74.8090366,44.7020304 C77.7303211,47.6298263 77.7303211,52.3701737 74.8090366,55.2979696 L31.4841462,98.7194157 L31.4841462,98.7194157 C29.7707286,100.429414 26.9956601,100.426484 25.2858573,98.7128699 C23.7142539,97.1377635 23.583287,94.6687943 24.8929565,92.9447108 L25.2858573,92.4942056 L67.6822349,49.9975644 L25.2858573,7.50579436 C23.5713809,5.78749646 23.5713809,3.00542802 25.2858573,1.28713012 C26.8531766,-0.283682641 29.3156149,-0.417046268 31.0349049,0.888769357 Z"
                id="Path"
              />
            </g>
          </g>
        </svg>
      </span>
    </Button>
  );
};

const useStyles = makeStyles((theme, bgCol) => ({
  wrapper: {
    borderRadius: "35rem",
    lineHeight: "15px",
    padding: "0.8rem 1.8rem",
    border: "1px transparent",
    minWidth: "8rem",
    outline: "none",
    verticalAlign: "middle",
    textAlign: "center",
    position: "relative",
    overflow: "hidden",
    cursor: "pointer",
    transition: "all .2s ease !important",
    position: "relative",

    "& span": {
      transition: "all .2s ease !important",
      height: "100%",
    },

    "& svg": {
      height: "18px",
      display: "inline-block",
      position: "absolute",
      transform: "translate(20px, 50px)",
      transition: "all .2s ease",
      willChange: "transform",
      top: "50%",
      transform: "translate3d(100px, -50%, 0)",
    },

    "&:hover": {
      backgroundColor: "#fff",
      "& span": {
        transform: "translate3d(-5px, 0, 0)",
      },

      "& svg": {
        transform: "translate3d(-10px, -50%, 0)",
      },
    },
  },
}));

// Map the state of the store to the props to be passed
// to the container component.
const mapStateToProps = (state) => ({
  // navDark: state.navReducer.navDark,
});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({
  bumpIt: () =>
    dispatch({
      type: "DARK",
    }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomButton);
