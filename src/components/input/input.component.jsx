import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSpring, animated } from "react-spring";
import { connect } from "react-redux";

import "./input.component.scss";

const CTAInput = (props) => {
  const classes = useStyles();

  const calcEmailValid = () => {
    return props.email.includes("@") && props.email.includes(".");
  };
  //const emailValid = calcEmailValid();

  const toggleButton = useSpring({
    transform: calcEmailValid() ? `translateX(0)` : `translateX(100%)`,
  });

  return (
    <form className={classes.root}>
      <input
        type="text"
        name="name"
        placeholder="Your email"
        value={props.email}
        className={classes.input}
        onChange={(event) => props.setEmail(event.target.value)}
      />

      <animated.button
        type="submit"
        className={classes.inputButton}
        style={toggleButton}
      >
        Lets Go
      </animated.button>
    </form>
  );
};

// Map the state of the store to the props to be passed
// to the container component.
const mapStateToProps = (state) => ({
  email: state.userReducer.email,
});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({
  setEmail: (email) =>
    dispatch({
      type: "UPDATE_EMAIL",
      payload: email,
    }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CTAInput);

const useStyles = makeStyles((theme) => ({
  root: {
    width: "30rem",
    display: "flex",
    border: "3px solid #fff",
    borderRadius: "1rem",
    overflow: "hidden",
    zIndex: 5,
    backgroundColor: "#fff",
    boxSizing: "border-box",
  },
  inputButton: {
    width: "40%",
    fontFamily: "din-condensed",
    fontSize: "1.3rem",
    fontStyle: "normal",
    fontWeight: "700",
    lineHeight: "15px",
    border: "none",
    letterSpacing: "1px",
    backgroundColor: "#A8A7FA",
    color: "#fff",
    cursor: "pointer",
    transition: "all .2s",

    "&:hover": {
      backgroundColor: "#7170A6",
    },

    "&:focus": {
      outline: "none",
    },
  },
  input: {
    border: "none",
    backgroundColor: "#fff",
    width: "100%",
    height: "100%",
    padding: ".8rem 1.5rem",
    fontSize: "1.3rem",
    fontFamily: "circle",
    fontWeight: "400",
    color: "#6A6A6A",

    "&:focus": {
      outline: "none",
    },
  },
}));
