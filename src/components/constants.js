// THEME COLORS
export const THEME_PRIMARY = "#A8A7FA";
export const THEME_SECONDARY = "#A8A7FA";

// OTHER COLORS
export const COL_WHITE = "#fff";

// Type
export const BUTTON_THEME_DARK = "dark";
export const BUTTON_THEME_LIGHT = "light";

// Social app links
export const SOCIAL_APP_LINKS = [
  {
    name: "Facebook",
    id: "a1",
    link: "https://www.swiftfoxapps.com",
  },
  {
    name: "Linked In",
    id: "a2",
    link: "https://www.swiftfoxapps.com",
  },
  {
    name: "YouTube",
    id: "a3",
    link: "https://www.swiftfoxapps.com",
  },
  {
    name: "Twitter",
    id: "a4",
    link: "https://www.swiftfoxapps.com",
  },
];

// Navbar menu links
export const NAVBAR_MENU_LINKS = [
  {
    name: "Products",
    id: "b1",
    link: "./Products",
  },
  {
    name: "Customers",
    id: "b2",
    link: "/Customers",
  },
  {
    name: "Design",
    id: "b3",
    link: "/Design",
  },
  {
    name: "Support",
    id: "b4",
    link: "/Support",
  },
];
