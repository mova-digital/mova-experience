import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { animated } from "react-spring";
import { FilledInput } from "@material-ui/core";
import { SOCIAL_APP_LINKS, NAVBAR_MENU_LINKS } from "../constants";

const NavMenu = ({ style }) => {
  const classes = useStyles();

  const [socialLinks, setSocialLinks] = useState(SOCIAL_APP_LINKS);

  const [menuLinks, setMenuLinks] = useState(NAVBAR_MENU_LINKS);

  return (
    <animated.div className={classes.root} style={style}>
      <Container container maxWidth="lg">
        <div className={classes.menuLinkRoot}>
          <div className={classes.menuLinkWrapper}>
            {menuLinks.map((menuLinks) => (
              <div className={classes.menuLink} key={menuLinks.id}>
                <Typography align="left" variant="h4" style={{ color: "#fff" }}>
                  {menuLinks.name}
                </Typography>
              </div>
            ))}
          </div>
        </div>

        <div className={classes.socialLinkRoot}>
          <div className={classes.socialLinkWrapper}>
            {socialLinks.map((social) => (
              <div className={classes.socialLink} key={social.id}>
                <a href={social.socialLink}>
                  <Typography
                    align="left"
                    variant="h5"
                    onMouseOver={() => console.log("clciked baby:")}
                    style={{ color: "#fff" }}
                  >
                    {social.name.toLowerCase()}
                  </Typography>
                </a>
              </div>
            ))}
          </div>
        </div>
      </Container>
    </animated.div>
  );
};

export default NavMenu;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    background: "#A8A7FA",
    zIndex: 10,
    position: "fixed",
    width: "100%",
    padding: "8rem 0 3rem 0",
    height: "100vh",
  },
  dropMenu: {
    zIndex: 10,
    padding: "2rem 0",
  },
  menuLinkRoot: {},
  menuLinkWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    padding: "0 3rem 5rem 0",
    [theme.breakpoints.down("sm")]: {
      padding: "0 0 5rem 0",
      alignItems: "center",
    },
  },
  menuLink: {
    cursor: "pointer",
    transition: "all .2s",
    padding: ".8rem 0",
    transitionTimingFunction: "ease-in",

    "&:hover": {
      transform: "translateX(1rem)",
      fontWeight: "600",
    },
  },
  socialLinkRoot: {
    width: "100%",
    display: "flex",
    justifyContent: "space-around",
  },
  socialLinkWrapper: {
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "flex-end",
    //backgroundColor: "#7DD895",
    [theme.breakpoints.down("sm")]: {
      justifyContent: "space-between",
      maxWidth: "25rem",
    },
  },
  socialLink: {
    paddingRight: "1rem",
  },
}));
