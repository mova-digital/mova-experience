import React, { useState, useLayoutEffect, useEffect } from "react";
import { useSpring, animated } from "react-spring";
import { makeStyles } from "@material-ui/core/styles";
import { THEME_PRIMARY, COL_WHITE } from "../constants";

import { connect } from "react-redux";

const openedTransformationConfig = {
  top: "translate(2, 7) rotate(0)",
  center: "translate(2, 19) rotate(0)",
  width1: "30",
  width2: "20",
};

const closedTransformationConfig = {
  top: "translate(5, 28) rotate(-45)",
  center: "translate(8, 7) rotate(45)",
  width1: "30",
  width2: "30",
};

const MenuIcon = (props, { isOpened }) => {
  const classes = useStyles();
  const { top, center, width1, width2 } = useSpring({
    to: props.navOpen ? closedTransformationConfig : openedTransformationConfig,
    // config: config.stiff,
  });

  let color = THEME_PRIMARY;

  if (props.navDark === true) {
    color = COL_WHITE;
  } else {
    color = THEME_PRIMARY;
  }

  return (
    <svg
      width="40"
      height="40"
      viewBox="0 0 44 30"
      fill={color}
      xmlns="http://www.w3.org/2000/svg"
      className={classes.root}
    >
      <animated.rect width={width1} height="3" rx="3" transform={top} />
      <animated.rect width={width2} height="3" rx="3" transform={center} />
    </svg>
  );
};

// Map the state of the store to the props to be passed
// to the container component.
const mapStateToProps = (state) => ({
  // navDark: state.navReducer.navDark,
  navOpen: state.navReducer.navOpen,
});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({
  bumpIt: () =>
    dispatch({
      type: "DARK",
    }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuIcon);

const useStyles = makeStyles((theme) => ({
  root: {
    zIndex: 10,
    cursor: "pointer",
  },
}));
