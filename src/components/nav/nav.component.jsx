import React, { useState } from "react";
import { useSpring, animated } from "react-spring";

import NavMenu from "./nav-menu";
import MenuIcon from "./hamburger";
import Logo from "../../assets/logos/logo.svg";
import CustomButton from "../button/button";

import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";

import { COL_WHITE } from "../constants";
import { THEME_PRIMARY } from "../constants";
import { THEME_SECONDARY } from "../constants";

import { connect } from "react-redux";

const Nav = (props) => {
  const classes = useStyles();

  const handleToggle = (props) => {
    if (props.navOpen === true) {
      props.setNavClosed();
      // props.setLight();
      //prevetScroll();
    } else {
      props.setNavOpen();
    }
  };

  if (props.navDark === "true") {
    console.log("dark");
  } else {
    console.log("light");
  }

  const navAnimation = useSpring({
    transform: props.navOpen ? `translate3d(0,0,0)` : `translate3d(0,-100%,0)`,
  });

  function prevetScroll() {
    if (props.navOpen === false) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "visible";
    }
  }

  //const fancyCount = `The count that is fancy is ${props.count}`;

  function computeLightVsDark(props) {
    const { page, navOpen } = props;

    const pagesWithDarkTheme = [0];

    if (pagesWithDarkTheme.includes(page) || navOpen) {
      return true;
    }

    return false;
  }

  const isDark = computeLightVsDark(props);

  return (
    <div>
      <AppBar
        position="fixed"
        color={"transparent"}
        elevation={0}
        style={{ zIndex: 20000 }}
      >
        <Container container maxWidth="lg">
          <div className={classes.nav}>
            <img src={Logo} className={classes.logo} />

            <div className={classes.navRight}>
              <div className={classes.contact}>
                <Typography
                  align="center"
                  style={
                    !isDark ? { color: THEME_PRIMARY } : { color: COL_WHITE }
                  }
                  variant="h6"
                >
                  +353 256 1080
                </Typography>
              </div>

              <div className={classes.navButton}>
                <CustomButton navDark={isDark}>Log in</CustomButton>
              </div>
              <div
                onKeyPress={() => handleToggle(props)}
                onClick={() => handleToggle(props)}
              >
                <MenuIcon navDark={isDark} />
              </div>
            </div>
          </div>
        </Container>
      </AppBar>
      <NavMenu style={navAnimation} />
    </div>
  );
};

// Map the state of the store to the props to be passed
// to the container component.
const mapStateToProps = (state) => ({
  navOpen: state.navReducer.navOpen,
  page: state.pageReducer.page,
});

// Map actions to properties in the container
const mapDispatchToProps = (dispatch) => ({
  setDark: () =>
    dispatch({
      type: "DARK",
    }),
  setLight: () =>
    dispatch({
      type: "LIGHT",
    }),
  setNavOpen: () =>
    dispatch({
      type: "OPEN",
    }),
  setNavClosed: () =>
    dispatch({
      type: "CLOSED",
    }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    zIndex: 10,
  },
  nav: {
    height: "4rem",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  navRight: {
    display: "flex",
    alignItems: "middle",
  },
  contact: {
    display: "flex",
    alignItems: "center",
    padding: "0 20px",
  },
  navButton: {
    marginRight: "2rem",
  },
  logo: {
    width: "10rem",
  },
}));
