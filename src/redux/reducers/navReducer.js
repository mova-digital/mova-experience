const initialState = { navDark: true, navOpen: false };

export default (state = initialState, action) => {
  switch (action.type) {
    case "DARK":
      return { ...state, navDark: true };
    case "LIGHT":
      return { ...state, navDark: false };
    case "OPEN":
      return { ...state, navOpen: true };
    case "CLOSED":
      return { ...state, navOpen: false };
    default:
      return state;
  }
};
