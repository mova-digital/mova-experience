const page = { page: 0 };

export default (state = page, action) => {
  switch (action.type) {
    case "UPDATE_PAGE":
      return { page: action.payload };
    default:
      return state;
  }
};
